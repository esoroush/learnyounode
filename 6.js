var m = require("./modeule_6")

if(process.argv != 4){
    console.error("You must provide exactly 2 argument for dirName and extension");
    
}
m(process.argv[2], process.argv[3], function(error, response) {
    if(error){
        console.error(error);
    }
    console.log(response.toString().split(",").join("\n"));
})