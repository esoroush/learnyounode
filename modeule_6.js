var fs = require("fs")

module.exports = function(dirName, extension, callback){
    fs.readdir(dirName, function(error, response) {
        if(error){
            return callback(error);
        }
        ret=[]
        for(i=0; i<response.length; ++i){
            if(response[i].endsWith("."+extension)){
                ret.push(response[i])
            }
        }
        return callback(null, ret)
    })
}