var http = require('http')
var bl = require('bl')                                                      
var results = []                                                     
var count = 0
                  
// why result array when acts like this!
// when I use a global for, results are always empty
// but whe I use it in httpGet function it acts normal as I expect it.                                                                                                                                                                                             }                                                                          
function httpGet(i) {
// for(i=0; i<3; ++i){
    http.get(process.argv[2+i], function(response){
        response.pipe(bl(function(error, data){
            if(error){
                console.error(error);
            }
            results[i]=data.toString();
            count++;
            if(count == 3){
                results.forEach(function(value){
                    console.log(value);
                })
            }
        }))
    })
}
for(j=0; j<3; ++j){
    httpGet(j);
}