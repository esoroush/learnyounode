# Learnyounode
My solution for *learnyounode* work shop at [nodeschool](https://nodeschool.io/).  
To get started run the following command:  
```bash
npm install -g learnyounode
```