var http = require("http")
var map = require("through2-map")

portNumber=Number(process.argv[2])
var server = http.createServer(function(request, response){
    if(request.method != "POST"){
        response.end("Send me a post request!");
    }
    request.pipe(map(function(chunk){
        return chunk.toString().toUpperCase()
    })).pipe(response)
})

server.listen(portNumber)