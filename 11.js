var http = require("http")
var fs = require("fs")


portNumber = Number(process.argv[2])
textFile = process.argv[3]
stream=fs.createReadStream(textFile)
var server = http.createServer(function(request, response){
    response.writeHead(200, {"content-type":"text/plain"})
    stream.pipe(response)
})
server.listen(portNumber)