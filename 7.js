var http = require("http")
try {
    url = process.argv[2]
    
} catch (error) {
    console.log("Insufficient argument.", error);
}
http.get(url,function(response) {
    response.setEncoding("utf8");
    response.on("data", console.log);
    response.on("error", console.error)
}).on("error", console.error)