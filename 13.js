var http = require("http")
var url = require("url")

portNumber = Number(process.argv[2])

var server = http.createServer(function(request, response){
    if(request.method != "GET"){
        response.end("Please send me GET method!")
    }
    
    const parsed=url.parse(request.url)
    iso=parsed.query.split('=')[1]
    pathName=parsed.pathname
    date = (new Date(iso))
    result=null
    if(pathName.endsWith("parsetime")){
        result={"hour":date.getHours(),
                "minute":date.getMinutes(),
                "second":date.getSeconds()}
    } else if(pathName.endsWith("unixtime")){
        result={"unixtime":date.getTime()}
    }
    if(result){
        response.writeHead(200, {"content-type":"application/json"})
        response.end(JSON.stringify(result))
    } else {
        response.writeHead(404)
    }

})

server.listen(portNumber)